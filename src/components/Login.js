import React from "react";

export const Login = () => {
  return (
    <div className="login-container">
      <div className="login-content">
        <div className="login-facebook-text">
          <div className="login-facebook-noti">
            <ul className="login-facebook-item">
              <li className="login-facebook-noti-name">facebook</li>
              <li className="login-facebook-noti-share">
                Facebook giúp bạn kết nối và chia sẻ
                <br />
                với mọi người trong cuộc sống của bạn.
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="login-form">
        <div className="login-form-input">
          <div className="login-form-input-detail">
            <input placeholder=" Email hoặc số điện thoại" />
          </div>
          <div className="login-form-input-detail">
            <input placeholder=" Mật khẩu" />
          </div>
          <div className="login-form-input-btn">
            <button>Đăng nhập</button>
          </div>
          <a className="login-form-forgot-question" href="#">
            Quên mật khẩu?
          </a>
          <div className="login-form-cross">
            <div className="login-form-cross-brick"></div>
          </div>
          <div className="login-form-input-create">
            <button>Tạo tài khoản mới</button>
          </div>
          <div className="login-form-page">
            <ul className="login-form-page-create">
              <li>
                <a href="">Tạo Trang</a> dành cho người nổi tiếng, thương hiệu
                hoặc doanh
                <br />
                <span>nghiệp.</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
