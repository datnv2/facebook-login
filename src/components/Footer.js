import React from "react";

export const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-list">
        <ul className="footer-list-item">
          <li>
            <a href="">Tiếng Việt</a>
          </li>
          <li>
            <a href="">English (UK)</a>
          </li>
          <li>
            <a href="">中文(台灣)</a>
          </li>
          <li>
            <a href="">한국어</a>
          </li>
          <li>
            <a href="">日本語</a>
          </li>
          <li>
            <a href="">Français (France)</a>
          </li>
          <li>
            <a href="">ภาษาไทย</a>
          </li>
          <li>
            <a href="">Español</a>
          </li>
          <li>
            <a href="">Português (Brasil)</a>
          </li>
          <li>
            <a href="">Deutsch</a>
          </li>
          <li>
            <a href="">Italiano</a>
          </li>
          <li>
            <a href="" role="button" className="footer-square">
              <i className="fas fa-plus" />
            </a>
          </li>
        </ul>
      </div>
      <div className="footer-bold">
        <div className="footer-bold-underlined"></div>
      </div>
      <div className="footer-list-register">
        <ul className="footer-list-register-item">
          <li>
            <a href="">Đăng ký</a>
          </li>
          <li>
            <a href="">Đăng nhập</a>
          </li>
          <li>
            <a href="">Messenger</a>
          </li>
          <li>
            <a href="">Facebook Lite</a>
          </li>
          <li>
            <a href="">Watch</a>
          </li>
          <li>
            <a href="">Địa điểm</a>
          </li>
          <li>
            <a href="">Trò chơi</a>
          </li>
          <li>
            <a href="">Marketplace</a>
          </li>
          <li>
            <a href="">Facebook Pay</a>
          </li>
          <li>
            <a href="">Oculus</a>
          </li>
          <li>
            <a href="">Portal</a>
          </li>
          <li>
            <a href="">Instagram</a>
          </li>
          <li>
            <a href="">Bulletin</a>
          </li>
        </ul>
      </div>
      <div className="footer-list-service">
        <ul className="footer-list-service-item">
          <li>
            <a href="">Địa phương</a>
          </li>
          <li>
            <a href="">Chiến dịch gây quỹ</a>
          </li>
          <li>
            <a href="">Dịch vụ</a>
          </li>
          <li>
            <a href="">Trung tâm thông tin bỏ phiếu</a>
          </li>
          <li>
            <a href="">Nhóm</a>
          </li>
          <li>
            <a href="">Giới thiệu</a>
          </li>
          <li>
            <a href="">Tạo quảng cáo</a>
          </li>
          <li>
            <a href="">Tạo Trang</a>
          </li>
          <li>
            <a href="">Nhà phát triển</a>
          </li>
          <li>
            <a href="">Tuyển dụng</a>
          </li>
        </ul>
      </div>
      <div className="footer-list-cookie">
        <ul className="footer-list-cookie-item">
          <li>
            <a href="">Quyền riêng tư</a>
          </li>
          <li>
            <a href="">Cookie</a>
          </li>
          <li>
            <a href="">Lựa chọn quảng cáo</a>
          </li>
          <li>
            <a href="">Điều khoản</a>
          </li>
          <li>
            <a href="">Trợ giúp</a>
          </li>
        </ul>
      </div>
      <div className="footer-list-meta">
        <p>Meta © 2022</p>
      </div>
    </div>
  );
};

export default Footer;
