import "./App.css";
import Login from "./components/Login.js";
import Footer from "./components/Footer.js";

function App() {
  return (
    <div>
      <Login />
      <Footer />
    </div>
  );
}

export default App;
